CLONE: (and give apache control!)
```
git clone https://trollderiu@bitbucket.org/trollderiu_projects/click-to-vote_web.git clicktovote
git clone https://trollderiu@bitbucket.org/trollderiu_projects/imagevote_core.git clicktovote/core
git clone https://trollderiu@bitbucket.org/trollderiu_projects/click-to-vote.git clicktovote/click-to-vote
git clone https://trollderiu@bitbucket.org/trollderiu_projects/imagevote_public.git clicktovote/click-to-vote/~commons
chown -R www-data:www-data clicktovote

cp -R clicktovote clicktovote_builder
chown -R www-data:www-data clicktovote_builder
```
CREATE 'config.php' FILE WITH LIKE:
```
#!php
$_APPNAME = "clicktovote";
$connect = "mysql:host=<IP>;port=3306;dbname=$_APPNAME";
$user = "<USER>";
$pass = "<PASS>";
```
PULL:
```
sh pull.sh
```

PRE-LAUNCH TEST GUIDE:

on builder (android browser):
//- run minify.bat
- git commit + push all in "<APP>"
- git pull all on "var/www/html/<APP>_builder" (WARNING!)

- Clean cache
- Load "<APP>-builder.tk" (select language + quit tutorial)
- Fill and Share new votation
- Click some shared poll "<APP>-builder.tk/<KEY>"
- Check click answer callback works on 'Play'

on Android (android browser)
- remove android app
- Click some shared poll "<APP>-builder.tk/<KEY>"
- click option to share it 
- click ok on use app and check redirection
- go back, and click again and check image creation

on Android (app):
- git pull all in "assets"
- Android Studio build and run

- Click 'Play' and 'Share' (check image + link)
- Vote and 'Share' again (memorize votations numbers)
- Change language and 'Share' again
- Change original language and check number votations are correct
- Click the link and open with app
- Click same answer again
- Click SKIP
- Move forward and backwards (check there is not jumps)
- Click the link and open with browser

- Go back, clicking New
- Click "show my polls" + "hide my polls"
- Click some poll
- Vote
- Click back (is poll updated?)

//on iPohne:
//TODO

upload:
- check core update will not affect old android/iphone version users

- git push/pull android version
- Change manifest version
- Build signed apk
- Upload in "play.google.com/apps/publish"

- git pull on production server

- git push android project
- Create branch in Bitbucket with the version 0.* for: "<APP>_android", "android", "<APP>_web", "<APP>", "imagevote_public", "core"